from django.utils.translation import gettext_lazy as _
from wagtail.blocks import StructBlock
from wagtail_bs_blocks.blocks.cards.base_card_block import BaseCardBlock


class FormBlock(StructBlock):

    class Meta:
        icon = 'cogs'
        label = _('Form Block')
        template = 'wagtail_bs_forms/blocks/form_block.html'


class FormCardBlock(BaseCardBlock):
    class Meta:
        icon = 'cogs'
        label = _('Form Card Block')
        template = 'wagtail_bs_forms/blocks/form_card_block.html'
