import logging
from django.db import models
from django.utils.translation import gettext_lazy as _
from modelcluster.fields import ParentalKey
from wagtail.admin.panels import (
        TabbedInterface,
        FieldPanel,
        InlinePanel,
        ObjectList
)
from wagtail.models import Page
from wagtail.fields import StreamField
from wagtail.contrib.forms.panels import FormSubmissionsPanel
from wagtailmenus.models import MenuPageMixin
from wagtailmenus.panels import menupage_panel
from .honeypot_form_mixin import HoneypotFormMixin
from .landing_form_mixin import LandingFormMixin
from .captcha_form_mixin import CaptchaFormMixin
from .email_form_mixin import EmailFormMixin
from .process_form_mixin import ProcessFormMixin
from .managed_form_handler import ManagedFormHandler, ManagedFormField
from ..forms import ManagedFormBuilder
from ..blocks import FormCardBlock, FormBlock
from wagtail_bs_blocks.blocks import get_layout_blocks
from wagtail_bs_blocks.blocks import TitleHeaderBlock


logger = logging.getLogger(__name__)


class ComposedFormField(ManagedFormField):

    page = ParentalKey('ComposedForm',
                       on_delete=models.CASCADE,
                       related_name='form_fields')


class ComposedForm(
    LandingFormMixin,
    EmailFormMixin,
    HoneypotFormMixin,
    CaptchaFormMixin,
    ProcessFormMixin,
    MenuPageMixin,
    ManagedFormHandler,
):

    class Meta:
        verbose_name = _('Composed Form Bootstrap Page')


    form_builder = ManagedFormBuilder

    form_content = StreamField(get_layout_blocks(
        extra_blocks=[('form_title', TitleHeaderBlock()),('form_card_block', FormCardBlock()),]),
        blank=False, use_json_field=True)

    content_panels = ManagedFormHandler.content_panels + [
        FormSubmissionsPanel(),
    ]

    settings_panels = ManagedFormHandler.settings_panels + [
            menupage_panel
            ]
    
    promote_panels = Page.promote_panels

    form_panels = ProcessFormMixin.process_content + \
            HoneypotFormMixin.honeypot_content + \
            CaptchaFormMixin.captcha_content + \
            EmailFormMixin.email_content + \
            LandingFormMixin.landing_content
      
    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Main'),
        ObjectList([InlinePanel('form_fields', label="Form fields")], heading='Form Fields'),
        ObjectList([FieldPanel('form_content')], heading='Form Layout'),
        ObjectList(form_panels, heading='Form Settings'),
        ObjectList(promote_panels, heading='Promote'),
        ObjectList(settings_panels, heading='Settings', classname="settings"),
        ])
