import logging
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import render
from django.contrib import messages
from wagtail.contrib.forms.models import AbstractForm, AbstractFormField
from wagtail_bs_forms import settings
from ..utils import process_data
from ..forms import ManagedSubmissionsListView, FORM_FIELD_CHOICES

logger = logging.getLogger(__name__)


class CallbackList(list):
    def fire(self, *args, **kwargs):
        logger.debug('callbacks %s', str(self))
        for listener in self:
            result = listener(*args, **kwargs)
            if result:
                return result


class ManagedFormField(AbstractFormField):

    class Meta:
        abstract = True
        ordering = ('sort_order',)

    field_type = models.CharField(
        verbose_name=_('field type'),
        max_length=16,
        choices=FORM_FIELD_CHOICES,
        blank=False,
        default='Single line text'
    )


class ManagedFormHandler(AbstractForm):

    class Meta:
        abstract = True

    submissions_list_view_class = ManagedSubmissionsListView

    encoder = DjangoJSONEncoder

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        logger.debug('init: %s', self.title)
        self.append_fields = {}
        self.post_before_callback = CallbackList()
        self.post_after_callback = CallbackList()

    def get_form_class(self):
        """override AbstractForm.get_form_class
        """
        logger.debug('get_form_class')
        # dynamically call subclass methods that register
        for base in type(self).__bases__:
            for attr in dir(base):
                if attr.startswith('register_'):
                    getattr(self, attr)()

        fb = self.form_builder(self.get_form_fields())
        for key, value in self.append_fields.items():
            fb.append_field(key, value)
        return fb.get_form_class()

    def serve(self, request, *args, **kwargs):
        logger.debug('serve %s', request.method)
        if request.method == 'POST':
            response = self.form_post_request(request)
            if response:
                return response
        return self.form_get_request(request)

    def form_get_request(self, request, form=None):
        logger.debug('form_get_request')
        if not form:
            form = self.get_form(page=self, user=request.user)
        context = self.get_context(request)
        context['form'] = form
        response = render(
            request,
            self.get_template(request),
            context
        )
        return response

    def form_post_request(self, request):
        logger.debug('form_post_request')
        form = self.get_form(request.POST, request.FILES, page=self, user=request.user)
        if not form.is_valid():
            return self.form_get_request(request, form)
        before = self.form_post_before_process(form, request)
        if before:
            return before
        processed_data = process_data(form, request)
        after = self.form_post_after_process(form, request, processed_data)
        if after:
            return after

        messages.success(request, settings.SUCCESS_MESSAGE)

        form_submission = self.process_form_submission(form)
        return self.render_landing_page(request, form_submission)

    def form_post_before_process(self, form, request):
        logger.debug('form_post_before_process')
        response = self.post_before_callback.fire(form, request)
        logger.debug('response:%s', response)
        for key, value in self.append_fields.items():
            form.fields.pop(key, None)
            form.cleaned_data.pop(key, None)
        return response

    def form_post_after_process(self, form, request, processed_data):
        logger.debug('form_post_after_process')
        return self.post_after_callback.fire(form, request, processed_data=processed_data)

    def serve_submissions_list_view(self, request, *args, **kwargs):
        logger.debug('serve_submissions_list_view')
        """
        Returns list submissions view for admin.

        `list_submissions_view_class` can be set to provide custom view class.
        Your class must be inherited from SubmissionsListView.
        """
        view = self.submissions_list_view_class.as_view()
        return view(request, form_page=self, *args, **kwargs)
