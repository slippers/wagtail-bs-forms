import logging
from django.db import models
from django.utils.translation import gettext_lazy as _
from modelcluster.models import ClusterableModel
from wagtail.admin.panels import (
    FieldPanel,
    MultiFieldPanel,
)
from wagtail.admin.mail import send_mail
from ..utils import data_to_dict

logger = logging.getLogger(__name__)


class EmailFormMixin(ClusterableModel):

    class Meta:
        abstract = True

    email_enabled = models.BooleanField(
        default=False,
        verbose_name=_('Enable sending email.'),
        help_text=_('When enabled, email will be sent.')
    )

    to_address = models.CharField(
        verbose_name=_('to address'),
        max_length=255,
        blank=True,
        help_text=_("Optional - form submissions will be emailed to these addresses.\
                    Separate multiple addresses by comma.")
    )

    from_address = models.CharField(
        verbose_name=_('from address'),
        max_length=255,
        blank=True
    )

    subject = models.CharField(verbose_name=_('subject'), max_length=255, blank=True)

    email_content = [
        MultiFieldPanel(
            [
                FieldPanel('email_enabled'),
                FieldPanel('to_address'),
                FieldPanel('from_address'),
                FieldPanel('subject'),
            ],
            _('Email')
        ),
    ]

    def register_email(self):
        logger.debug('register_email')
        if self.email_enabled:
            self.post_after_callback.append(self.send_mail)

    def send_mail(self, form, request, processed_data=None):
        logger.debug('send_mail')
        if not self.to_address:
            logger.error('send_mail to_address not configured.')
            return
        addresses = [x.strip() for x in self.to_address.split(',')]
        content = []
        for key, value in data_to_dict(processed_data, request).items():
            content.append('{0}: {1}'.format(
                key.replace('_', ' ').title(),
                value
            ))
        content = '\n-------------------- \n'.join(content)

        send_mail(self.subject, content, addresses, self.from_address,)
