import logging
from django.contrib import messages
from django.db import models
from django.forms.widgets import TextInput
from django.forms.fields import CharField
from django.utils.translation import gettext_lazy as _
from modelcluster.models import ClusterableModel
from wagtail.admin.panels import (
    FieldPanel,
    MultiFieldPanel,
)


logger = logging.getLogger(__name__)


class HoneypotTextInput(TextInput):
    """
    create an invisible field
    """
    template_name = 'wagtail_bs_forms/honeypot_field.html'


class HoneypotFormMixin(ClusterableModel):

    class Meta:
        abstract = True

    honeypot_enabled = models.BooleanField(
        default=False,
        verbose_name=_('Spam Protection via honeypot input'),
        help_text=_('When enabled, the CMS will filter out spam form submissions for this page.')
    )

    spam_honeypot_field_name = models.CharField(
        max_length=255,
        default='hidden-dragon-comment',
        verbose_name=_('Honeypot input field name.'),
        help_text=_('A valid html input field name to be used as a honeypot.'),
    )

    honeypot_content = [
        MultiFieldPanel(
            [
                FieldPanel('honeypot_enabled'),
                FieldPanel('spam_honeypot_field_name')
            ],
            _('Honeypot spam protection.')
        ),
    ]

    def register_honeypot(self):
        logger.debug('register_honeypot')
        if self.honeypot_enabled:
            self.post_before_callback.append(self.honeypot)
            self.append_fields[self.spam_honeypot_field_name] = \
                        CharField(label='', required=False, widget=HoneypotTextInput)

    def honeypot_message(self, request, form):
        logger.info("Honeypot active on page: {0}\n{1}".format(self.title, request.POST))
        # use a config value here.
        msg = _("There was an error while processing your submission.  Please try again.")
        messages.error(request, msg)

    def honeypot(self, form, request):
        logger.debug('honeypot')
        if self.honeypot_enabled:
            if request.POST.get(self.spam_honeypot_field_name, None):
                self.honeypot_message(request, form)
                return self.form_get_request(request, form)
