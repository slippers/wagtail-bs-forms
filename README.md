# wagtail boostrap forms

## Intro

This package is a composition of wagtail and django form technologies. 
Some features are taken directly from the wagtail.contrib.forms namespace.  
Others were acquired from projects like CodeRedCms.

There are so many different requirements when posting a form.  Each mixin
addresses its problem space through model fields, appended django fields 
and before/after callbacks. Ultimatly when a form is constructed with get 
or posted the mixin become involved in processing.

## Requirements

Introduce dynamic form input fields that are not tied to a data model.

On successful form posting apply one or more before and after process callbacks.

Allow for Form Page composition where features can be easily removed or new ones added.

Offload all unique processing logic to the Mixin subclass.

Logging

Address basic bootstrap input type issues.

Solve for honeypot, captcha, email, and landing page logic.

Solve for basic roadmap of adding new form features via mixins.

setttings are centralized and cached.

Form as block in layout via wagtail-bs-blocks

## Settings

    WAGTAIL_BS_FORMS_SUCCESS_MESSAGE
    WAGTAIL_BS_FORMS_PROTECTED_MEDIA_URL
    WAGTAIL_BS_FORMS_PROTECTED_MEDIA_ROOT
    WAGTAIL_BS_FORMS_PROTECTED_MEDIA_UPLOAD_WHITELIST
    WAGTAIL_BS_FORMS_PROTECTED_MEDIA_UPLOAD_BLACKLIST

## FormHandler

The main class that handles get and post is FormHandler.   FormHandler is an
AbstractForm from the wagtail.contrib.forms namespace. This was done to
attempt to keep the knowledge space as centered on AbstractForm.

FormHandler has no model fields itself and expects to get them from one or more mixin
classes specified as a subclass of your form page.  

### Features

Creating a method that starts with `register_` in each mixin supplies the needed linkage
to FormHandler. This register method can interact with FormHandler in this way. 

Appending Django fields to your form is a requirement.  Input fields that are appended
are not tied to the data back end but simply included into the form.  
When the form is processed these fields are removed.

Before and after processing of the form are facilitated via callbacks. 
When the mixin must supply a 

## ComposedForm and ComposedFormField

ComposedForm and ComposedFormField are provided as a default page to use. 
Remove and add mixins to ComposedForm get the features you want.
    
    class ComposedForm(
               LandingFormMixin,
               EmailFormMixin,
               HoneypotFormMixin,
               CaptchaFormMixin,
               ProcessFormMixin,
               ManagedFormHandler,
              ):

## MangedFormBuilder

This customized FormBuilder class addressed input field creation for form fields.

Override the `form_builder` property in your page class. 
    
    form_builder = ManagedFormBuilder

## Settings

    SUCCESS_MESSAGE
    PROTECTED_MEDIA_URL
    PROTECTED_MEDIA_ROOT
    PROTECTED_MEDIA_UPLOAD_WHITELIST
    PROTECTED_MEDIA_UPLOAD_BLACKLIST

## Mixins

Each mixin supplies one or model.Model fields.  The default ManagedForm exposes these
mainly on the settings panel. These fields get used in the processing logic and don't
necessarily show up on the form page. 

### process form

ProcessFormMixin processes the submitted data and creates a form_submission class.

Persisting to database.

Calling hooks


### bootstrap4 and forms

django-bootstrap4 has been introduced to the templates.  some adjustments
were needed to properly expose some form input types like date.

    form_builder = ManagedFormBuilder

messages such as errors are generated so be sure to display messages in
your templates.

    {% load bootstrap4 %}
    <div class="container">
        {% bootstrap_messages %}
    </div>

### honeypot input spam protection

honeypot refers to a technique of supplying a hidden form input
that only a dumb robot would fill out.  if that field is supplied
then the from submission is rejected.  The input field is visible 
having a style applied to make it invisible.

HoneypotFormMixin supplies the honeypot field and admin
fields that control it.  The input field name can be altered
from the admin.

honeypot_content needs to be appended to your content_panels

### Landing Page

The LandingPageFormMixin supplies a pagechooser field.
When supplied a valid internal page upon form completion
the user will be redirected there.

landing_content needs to be appended to your content_panels

### Captcha as optional spam protection

CaptchaFormMixin supplies the activation of a captcha field.

django-simple-captcha needs this url added to urls.py

    url(r'^captcha/', include('captcha.urls')),

### Email

Mixin:

    EmailFormMixin

Django:

[mail](https://docs.djangoproject.com/en/3.0/topics/email/) is provided 
by the Django mail backend.

Smtpd:

django has backend handlers for email. including one for dev.

use the python smtp test server to send email local

[fake smtp server](https://muffinresearch.co.uk/fake-smtp-server-with-python/)

    sudo python -m smtpd -n -c DebuggingServer localhost:25
